$Id

Overview
--------
This module allows Drupal to replace textarea input fields with the Java-based standards-compliant edit-on Pro WYSIWYG editor.


Required components
-------------------
To use XStandard in Drupal you need to download edit-on Pro from RealObjects: http://www.realobjects.com/edit-on_Pro_4.850.0.html
Note that to disable demo mode you need to acquire a license key.


Installing the editor
---------------------
1. Download and uncompress the edit-on Pro package from RealObjects and copy the "eopro" folder into the edit-on Pro module folder.

2. In Drupal, go to "Administer" -> "Site building" -> "Modules" and enable the edit-on Pro module.

4. Go to the settings page for the edit-on Pro module ("Administer" -> "Site configuration" -> "edit-on Pro") and configure the module. Note that if the field "Fields that should be edited with edit-on Pro" is empty the module will never be used. You can get the id values for the fields you want to enable edit-on Pro for by going to the form in question logged in as user 1. Under each textarea input field there is a line of text that reads something like "Use edit-on Pro to edit Something content by including this id: edit-something". Insert that id in the "Fields that should be edited with edit-on Pro" field.

5. To use all features in you need to use the "Full HTML" input filter. 

